package source.Game;

import source.Cells.Cell;
import source.Cells.CellStateChangeListener;
import source.Display.Display;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GameOfLife implements Runnable {


    public static void main(String... args) {
        GameOfLife game = new GameOfLife(3, 3);
        game.run();
    }

    private int turn = 0;
    private List<Cell> cells;
    private int height, width;
    private Display<Cell> display;
    private CellStateChangeListener stateChangeListener;


    public GameOfLife(List<Cell> cells, int width, int height) {
        this.width = width;
        this.height = height;
        this.display = new Display<>(cells, true);
        this.display.setDisplayer(point -> {
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(point.isAlive() ? "X" : " ").append("]");
            if ((point.getX() + 1) % width == 0) sb.append("\n");

            System.out.print(sb.toString());
        });

        stateChangeListener = cell -> display.updatePoint(cell);

        for (int i = 0; i < cells.size(); i++) {
            cells.get(i).setListener(stateChangeListener);
        }

        this.cells = cells;
    }

    public GameOfLife(int width, int height) {
        this(generateDefaultCells(height, width), width, height);
    }

    private static List<Cell> generateDefaultCells(int height, int width) {
        List<Cell> cells = new ArrayList<>();
        for (int i = 0; i < height; i++) {
            for (int n = 0; n < width; n++) {
                Cell c = new Cell(n, i);
                if ((i == 1 && n == 2) || (i == 2 && n == 2) || (i == 3 && n == 2) || (i == 3 && n == 1) || (i == 2 && n == 0))
                    c.setAlive(true);
                cells.add(c);
            }
        }
        return cells;
    }

    private void applyRules() {
        List<Cell> oldCells = createDeepCopy(cells);

        for (int i = 0; i < cells.size(); i++) {
            if (getLiveNeighboursCount(oldCells, oldCells.get(i)) < 2)
                cells.get(i).setAlive(false);
            if (getLiveNeighboursCount(oldCells, oldCells.get(i)) > 3)
                cells.get(i).setAlive(false);
            if (getLiveNeighboursCount(oldCells, oldCells.get(i)) == 3)
                cells.get(i).setAlive(true);
        }
        turn++;
    }

    private List<Cell> createDeepCopy(List<Cell> cells) {
        List<Cell> result = new ArrayList<>();
        for (Cell c : cells) {
            result.add((Cell) c.clone());
        }
        return result;
    }

    private boolean hasAlive() {
        return cells.stream().filter(cell -> cell.isAlive()).count() > 0;
    }

    private int getLiveNeighboursCount(List<Cell> cells2, Cell cell) {
        return getLiveNeighbours(cells2, cell).size();
    }

    private Cell getCell(List<Cell> cells, int x, int y) {
        return ((Cell) cells.stream().filter(cell -> cell.x == x && cell.y == y).toArray()[0]);
    }

    private List<Cell> getNeighbours(List<Cell> cells, Cell cell) {
        return getNeighbours(cells, cell.x, cell.y);
    }

    private List<Cell> getLiveNeighbours(List<Cell> cells, int x, int y) {
        return getNeighbours(cells, x, y).stream().filter(cell -> cell.isAlive()).collect(Collectors.toList());
    }

    private List<Cell> getLiveNeighbours(List<Cell> cells, Cell cell) {
        return getLiveNeighbours(cells, cell.x, cell.y);
    }

    private List<Cell> getNeighbours(List<Cell> cells, int x, int y) {
        List<Cell> result = new ArrayList<>();
        for (Direction d : Direction.values()) {
            result.add(getCell(cells, x, y, d));
        }
        return result;
    }

    public Cell getCell(int x, int y) {
        return getCell(cells, x, y);
    }

    private Cell getCell(int x, int y, Direction dir) {
        return getCell(cells, x, y, dir);
    }

    private Cell getCell(List<Cell> cells, int x, int y, Direction dir) {
        switch (dir) {
            case NORTH:
                y = normalizeHeight(y + 1);
                break;
            case WEST:
                x = normalizeWidth(x - 1);
                break;
            case SOUTH:
                y = normalizeHeight(y - 1);
                break;
            case EAST:
                x = normalizeWidth(x + 1);
                break;
            case NORTH_EAST:
                x = normalizeWidth(x + 1);
                y = normalizeHeight(y + 1);
                break;
            case NORTH_WEST:
                y = normalizeHeight(y + 1);
                x = normalizeWidth(x - 1);
                break;
            case SOUTH_EAST:
                y = normalizeHeight(y - 1);
                x = normalizeWidth(x + 1);
                break;
            case SOUTH_WEST:
                y = normalizeHeight(y - 1);
                x = normalizeWidth(x - 1);
                break;
            default:
                return null;
        }

        return getCell(cells, x, y);
    }

    private Cell getCell(List<Cell> cells, Cell start, Direction dir) {
        return getCell(cells, start.x, start.y, dir);
    }

    private int normalizeWidth(int width) {
        return (width + this.width) % this.width;
    }

    private int normalizeHeight(int height) {
        return (height + this.height) % this.height;
    }

    public Cell clickOnCell(int i) {
        return clickOnCell(cells.get(i));
    }

    public Cell clickOnCell(int x, int y) {
        return clickOnCell(getCell(x, y));
    }

    public Cell clickOnCell(Cell cell) {
        cell.setAlive(!cell.isAlive());
        return cell;
    }

    public List<Cell> getCells() {
        return cells;
    }

    @Override
    public void run() {
        int i = 0;
        while (hasAlive()) {
            try {
                System.out.println(i++ % 4 + 1);
                display.display();
                applyRules();
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
        display.display();
    }

    private enum Direction {
        WEST, EAST, NORTH, SOUTH, NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST;
    }
}
