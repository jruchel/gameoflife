package source.Game;

import source.Cells.Cell;

public interface GameDisplay {

    void display(Cell cell);

}
