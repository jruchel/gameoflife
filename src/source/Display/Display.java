package source.Display;


import source.Cells.Cell;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class Display<E extends Point> {

    private boolean displayWhole;
    private List<E> points;
    private Displayer<E> displayer;
    private boolean initiated = false;
    private List<E> updated;

    public Display(List<E> points, boolean displayWhole) {
        this.displayWhole = displayWhole;
        this.points = points;
        if (!displayWhole) {
            updated = new ArrayList<>();
        }
    }

    public void display() {
        if (!initiated || displayWhole) {
            points = twoDimensionalSort(points, true);
            points.stream().forEach(point -> displayer.display(point));
            initiated = true;
        } else {
            updated.forEach(updated -> displayer.display(updated));
            updated = new ArrayList<>();
        }
    }


    private List<E> twoDimensionalSort(List<E> points, boolean ascending) {
        int multiplier = ascending ? 1 : -1;
        List<List<E>> separated = new ArrayList<>();

        List<Integer> distinctYs = points.stream().map(e -> e.y).distinct().collect(Collectors.toList());
        distinctYs = distinctYs.stream().sorted(Comparator.comparingInt(o -> o * multiplier)).collect(Collectors.toList());

        for (Integer in : distinctYs) {
            separated.add(points.stream().filter(e -> e.y == in).collect(Collectors.toList()));
        }
        for (int i = 0; i < separated.size(); i++) {
            List<E> l = separated.get(i);
            separated.set(i, l.stream().sorted(Comparator.comparingInt(o -> o.x * multiplier)).collect(Collectors.toList()));
        }

        List<E> result = new ArrayList<>();

        separated.stream().forEach(list -> list.stream().forEach(e -> {
            result.add(e);
        }));

        return result;
    }

    public void createOscilator(int x, int y) {
        ((Cell) (points.stream().filter(point -> point.x == x && point.y == y).toArray()[0])).setAlive(true);
        ((Cell) (points.stream().filter(point -> point.x == x - 1 && point.y == y).toArray()[0])).setAlive(true);
        ((Cell) (points.stream().filter(point -> point.x == x + 1 && point.y == y).toArray()[0])).setAlive(true);
    }


    public void reset() {
        initiated = false;
    }

    public void setDisplayer(Displayer<E> displayer) {
        this.displayer = displayer;
    }

    public void updatePoint(E point) {
        if (displayWhole) {
            points.removeIf(e -> e.x == point.x && e.y == point.y);
            points.add(point);
        } else {
            updated.add(point);
        }
    }

    public interface Displayer<E extends Point> {
        void display(E point);
    }

}
