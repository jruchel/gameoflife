package source.Cells;

import java.awt.*;

public class Cell extends Point {

    private boolean alive;
    private  CellStateChangeListener listener;

    public Cell(int x, int y, boolean alive, CellStateChangeListener listener) {
        this.x = x;
        this.y = y;
        this.alive = alive;
        this.listener = listener;
    }

    public Cell(int x, int y, CellStateChangeListener listener) {
        this(x, y, false, listener);
    }

    public Cell(int x, int y) {
        this(x, y, false, cell -> System.out.println(cell.toString() + " changed state"));
    }


    public void setAlive(boolean alive) {
        if (alive != this.alive) {
            this.alive = alive;
            listener.cellStateChanged(this);
        } else {
            this.alive = alive;
        }

    }

    public boolean isAlive() {
        return alive;
    }

    public void setListener(CellStateChangeListener listener) {
        this.listener = listener;
    }

    @Override
    public Object clone() {
        return super.clone();
    }

    @Override
    public String toString() {
        return y + " " + x;
    }
}
