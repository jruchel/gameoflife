package source.Cells;

public interface CellStateChangeListener {
    void cellStateChanged(Cell cell);
}
