package tests;

import org.junit.Assert;
import org.junit.Test;
import source.Cells.Cell;
import source.Game.GameOfLife;

public class GameOfLifeTest {

    int size = 10;
    private GameOfLife SUT = new GameOfLife(size, size);

    @Test
    public void clickTestFirstRowFirst() {
        Cell expected = SUT.getCell(1, 0);
        Cell actual = SUT.clickOnCell(1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void clickTestSecondRowFirst() {
        Cell expected = SUT.getCell(0, 1);
        Cell actual = SUT.clickOnCell(size);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void clickTestLastRowFirst() {
        Cell expected = SUT.getCell(size - 1, size - 1);
        Cell actual = SUT.clickOnCell(size * size - 1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void clickTestFirstRowLast() {
        Cell expected = SUT.getCell(size - 1, 0);
        Cell actual = SUT.clickOnCell(size - 1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void clickTestSecondRowLast() {
        Cell expected = SUT.getCell(size - 1, 1);
        Cell actual = SUT.clickOnCell(size + size - 1);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void clickTestLastRowLast() {
        Cell expected = SUT.getCell(size - 1, size - 1);
        Cell actual = SUT.clickOnCell(size * size - 1);
        Assert.assertEquals(expected, actual);
    }
}
